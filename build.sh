DIST_DIR="./dist"

PYTHON_VENV="./venv"

PYTHON_VERSION="2.7.16"
PYTHON_VERSION_MAJOR_MINOR="$(echo "$PYTHON_VERSION" | cut -d'.' -f-2)"
PYTHON_PATH="$PYTHON_VENV/lib/python$PYTHON_VERSION_MAJOR_MINOR"

PYTHON_PACKAGES="$PYTHON_PATH/site-packages"

# Make sure to update this to your applications relative directory.
APP_DIR="APP_DIRECRORY/"
APP_NAME="$(basename "$APP_DIR")"

DEFAULT_PYTHON_VERSION="$(python -V 2>&1 | grep -Po '(?<=Python )(.+)')"

if [ "$DEFAULT_PYTHON_VERSION" == "$PYTHON_VERSION" ]
then
    if [ ! -d "$DIST_DIR" ]
    then
        mkdir "$DIST_DIR"
    fi;

    if [ -f "$DIST_DIR"/$APP_NAME.zip ]
    then
        rm "$DIST_DIR"/$APP_NAME.zip
    fi;

    if [ -d venv ]
    then 
        rm -r venv 
    fi;

    if [ ! -d "$PYTHON_VENV" ]
    then
        python -m virtualenv venv
    fi;

    "$PYTHON_VENV"/bin/pip install -r "$APP_DIR/requirements"

    cd $PYTHON_PACKAGES
    zip -gr "$OLDPWD/$DIST_DIR/$APP_NAME.zip" . -x *.git*
    cd $OLDPWD

    cd $APP_DIR
    zip -gr "$OLDPWD/$DIST_DIR/$APP_NAME.zip" . -x *.git*
    cd $OLDPWD
else
    echo "Python version $PYTHON_VERSION is required. Currently Installed: $DEFAULT_PYTHON_VERSION"
fi;