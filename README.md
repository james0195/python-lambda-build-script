This script will allow you to take your python application and build it for AWS.

You can modify the variables to locate python environments and packages.

Expected file hierarchy

.

|-- APP_DIR

|-- venv (Python Virtual Environment)

|-- build.sh (Build Script)

|-- build-mac.sh (Build Script for the mac os)

|-- Dockerfile (Used by build-mac.sh script)


## Mac Build

At times when building a package directly from mac you will face ELF errors when running Lambda functions.

In order to fix this you will need to build your python script using a linux environment.

If like me you would rather not spin up virtual environments using VBOX or vagrant there is a solution!

Using docker, albeit this is a virtual machine system it is much simpler and easier to generate.

With the added bonus of using once and trash after. This can be beneficial if we simply need a linux environment to build.

Scripts

* build-mac.sh
* Dockerfile

I am assuming that you have docker installed and running on your mac before you utilize those scripts.

## Usage

Clone Repo
```sh
$ git clone git@bitbucket.org:james0195/python-lambda-build-script.git
```

Grant execution permissions
```sh
$ chmod +x ./build.sh ./build-mac.sh
```

Build package
```sh
$ ./build.sh
```

Build package on Mac

> Uses the Dockerfile to build script in a python virtual environment (./venv)

```sh
$ ./build-mac.sh
```