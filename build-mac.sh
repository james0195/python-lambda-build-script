# This script will untilize docker to create a linux system to help build the package.
# Reason for this is if you use python modules installed on mac and move them to aws lambda
# - You will face the dreaded "invalid ELF header".
docker build -t lambda-python-build .
docker run --volume="$PWD":/App lambda-python-build -it --rm