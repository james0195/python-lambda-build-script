# Use this docker image to help with keeping OS Archetecture the same when building AWS lambda packages.
# - As AWS uses a linux system to deploy it's modules.
# - Since you cannot directly import and install packages with pip on AWS Lambda functions,
# - We create a stable linux environment to build from.
FROM debian

MAINTAINER jamezilla<jamezilla0@gmail.com>

RUN apt-get update -y
RUN apt-get install zip -y
RUN apt-get install python2.7 -y
RUN apt-get install python-pip -y
RUN pip install virtualenv

WORKDIR /App

ENTRYPOINT /bin/bash /App/build.sh